<?php
/*
* Include the required bootstrap files
*/
require 'Core/app.php';
require 'vendor/autoload.php';
require 'LoadCore.php';

// Initiate the kernel class
$app = new Init;

// Set the Environment
define('ENVIRONMENT', 'production');
if (defined('ENVIRONMENT'))
{
    switch (ENVIRONMENT)
    {
        case 'development':
            error_reporting(E_ALL);
            break;

        case 'testing':
        case 'production':
            error_reporting(0);
            break;

        default:
            exit('The application environment is not set correctly.');
    }
}

/**
 *Run checks for server path
 */
if (isset($_SERVER['PATH_INFO'])) {
    /**
     *Set var $path to Predefined $_SERVER['PATH_INFO']
        */
    $path = $_SERVER['PATH_INFO'];

    /**
     *Split result or Sever path request into Array
        *@return $path;
        */
    $server = $app->path_split($path);
}
/*
Assign default '/' if previous check result to False
*/
else {
        $server = "/";
        header('location: user/login');
}

switch (Init::is_slash($server)) {

    case true:

        require_once __DIR__.'/Models/UserModel.php';
        require_once __DIR__.'/Controllers/UserController.php';

        $app->req_model = new UserModel();
        $app->req_controller = new UserController($app->req_model);
        /**
         *Model and Controller assignment with first letter as UPPERCASE
        *@return Class;
        */
        $model = $app->req_model;
        $controller = $app->req_controller;

        /**
         *Creating an Instance of the the model and the controller each
        *@return Object;
        */
        $ModelObj = new $model;
        echo $app->req_model;die;
        $ControllerObj = new $controller($app->req_model);

        /**
         *Assigning Object of Class Init to a Variable, to make it Usable
        *@return Method Name;
        */
        $method = $app->req_method;

        /**
         *Check if Controller Exist is not empty, then performs an
        *action on the method;
        *@return true;
        */
        if ($app->req_method != '') {
            /**
             *Outputs The Required controller and the req *method respectively
            *@return Required Method;
            */
            print $ControllerObj->$method($app->req_param);
        } else {
            /**
             *This works in only when the Url doesnt have a parameter
            *@return void;
            */
            print $ControllerObj->index();
        }
        break;

    case false:

        /**
         *Set Required Controller name ;
         *@return controller;
         *
         */
        $app->req_controller = $server[1];

        /**
         *Set Required Model name
         *@return model;
         */
        $app->req_model = $server[1];

        /**
         *Set Required Method name
         *@return method;
         */
        $app->req_method = isset($server[2])? $server[2] :'';

        /**
         *Set Required Params
         *@return params;
         */
        $app->req_param = array_slice($server, 3);

        /**
         *Check if Controller Exist
         *@return void;
         */
        $req_controller_exists = __DIR__.'/Controllers/'.$app->req_controller.'Controller.php';

        if (file_exists($req_controller_exists))
        {
            /**
             *Requiring all the files needed i.e The Corresponding Model and Controller
             *@return corresponding class respectively;
             */
            require_once __DIR__.'/Models/'.$app->req_model.'Model.php';
            require_once __DIR__.'/Controllers/'.$app->req_controller.'Controller.php';

               /**
             *Model and Controller assignment with first letter as UPPERCASE
             *@return Class;
             */
            $model = ucfirst($app->req_model).'Model';
            $controller = ucfirst($app->req_controller).'Controller';

            /**
             *Creating an Instance of the the model and the controller each
             *@return Object;
             */
            $ModelObj = new $model;
            $ControllerObj = new $controller(ucfirst($app->req_model.'Model'));

               /**
             *Assigning Object of Class Init to a Variable, to make it Usable
             *@return Method Name;
             */
            $method = $app->req_method;
        
               /**
             *Check if Controller Exist is not empty, then performs an
             *action on the method;
             *@return true;
             */
            if ($app->req_method != '') {
                /**
                 *Outputs The Required controller and the req *method respectively
                *@return Required Method;
                */
                print $ControllerObj->$method($app->req_param);
            } else {
                /**
                 *This works in only when the Url doesnt have a parameter
                 *@return void;
                 */
                print $ControllerObj->index();
            }
        }
        else
        {
            header('HTTP/1.1 404 Not Found');
            die('404 - The file - '.$app->req_controller.' - not found');
            //require the 404 controller and initiate it
            //Display its view
        }
        break;

    default:
        print 'An Error Occured';
        break;
}
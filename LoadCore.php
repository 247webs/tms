<?php
function robot($cn) {
  $last = strpos($cn, '\\') + 1;
  $cn = substr($cn, $last);
  $dir = str_replace('\\', '/', $cn);
  $f = __DIR__ . '/' . $dir . '.php';
  require_once($f);
}
spl_autoload_register('robot');
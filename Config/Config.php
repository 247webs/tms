<?php 

/*
*---------------------------------------------------------------
* CustomFramework Config File 
*---------------------------------------------------------------
*
* Contains al the config details, should be git ignored
*/

Class Config {

    function __construct(){

        session_start();

        // Set Site Url
        $this->url                  = "https://localhost/tms/";

        // Set Upload Dir
        $this->dirSeparator          = "/";
        $this->uploadDirPO          = "uploads/";
        $this->uploadDirCC          = "uploads/company/";

        // Set Database Details
        $this->database_host        = "localhost";
        $this->database_username    = "root";
        $this->database_password    = "root";
        $this->database_name        = "task_manager";
        $this->database_port        = 3306;

        // Set RoleIds
        $this->rolePropertyOwner    = 1;
        $this->roleCleaningCompany  = 2;
    }
}
$(function() {
  /* variables */
  var status = $('.status');
  var percent = $('.percent');
  var bar = $('.bar');
  
  /* submit form with ajax request using jQuery.form plugin */
  $('.uploadFile').ajaxForm({

    /* set data type json */
    dataType:'json',

    /* reset before submitting */
    beforeSend: function() {
      status.fadeOut();
      bar.width('0%');
      percent.html('0%');
    },

    /* progress bar call back*/
    uploadProgress: function(event, position, total, percentComplete) {
      var pVel = percentComplete + '%';
      bar.width(pVel);
      percent.html(pVel);
    },

    /* complete call back */
    complete: function(data) {
      data = $.parseJSON(data.responseText);
      if(data.count == '0'){
        status.html('Files not uploaded!').fadeIn();
      }else{
        status.html(data.count + ' Files uploaded!').fadeIn();
      }
      setTimeout(function(){ location.reload(); }, 2000);
    }
  });
    
/* submit form with ajax request using jQuery.form plugin */
$('.assignTask').ajaxForm({

    /* set data type json */
    dataType:'json',


    /* complete call back */
    complete: function(data) {
      if(data.responseText == "success") {
        $('.assign_msg').text('Success!!');
        setTimeout(function(){ location.reload(); }, 500);
      }
    }
  });
});
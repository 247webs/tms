<?php
/** Autoloading The required Classes **/
require_once 'CoreController.php';

class UserController extends CoreController
{
    private $model;

    /**
     *@param $file;
     */         
      function __construct( $file ){

        parent::__construct();

        /** Loading the corresponding Model class **/
        $this->model = new $file;

        if ($this->isLoggedIn()) {
            $this->redirect('task');
        }
    }
    
    /**
     * Loading login view
     */
    public function login() {
        Init::view('login');
    }

    /**
     * varify and login user
     */
    public function auth() {
        $loggedIn = $this->model->login($this->request['email']);
        if ($loggedIn) {
            $this->redirect('task');
        }
        Init::view('login', ['message'=> 'Invalid Email address entered!']);
    }
}
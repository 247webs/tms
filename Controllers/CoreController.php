<?php

/** Autoloading The required Classes **/
require_once 'Config/Config.php';

class CoreController extends Config
{
    /*
    * Public Core Properties
    */
    public $user_id;
    public $email;
    public $role;
    public $request;

    /*
    * Private Core Properties
    */
    private $get;
    private $post;

    function __construct() {

        parent::__construct();

        $this->user_id = isset($_SESSION['user_id']) ? $_SESSION['user_id'] : null;
        $this->email = isset($_SESSION['email']) ? $_SESSION['email'] : null;
        $this->role = isset($_SESSION['role']) ? $_SESSION['role'] : null;

        // Filter the request parameters
        $this->filterRequest();
    }

    // Check if user is logged in
    function isLoggedIn() {
        if ($this->user_id && $this->email && $this->role) {
          return true;
        }
    }

    // redirect helper function
    function redirect($route) {
        header('location:' . $this->url . $this->getPath($route));
    }

    // Get Uri based on given route
    function getPath($route) {

        switch ($route) {
            case 'login':
                $path = 'user/login';
                break;
            
            case 'task':
                $path = 'task/index';
                break;
            
            default:
                $path = '';
                break;
        }
        return $path;
    }

    // Filter request parameters and make them usable
    function filterRequest() {

        $this->request = [];

        // Possible post requests parameters
        $this->post = filter_input_array(INPUT_POST, [
            "id" => FILTER_VALIDATE_INT,
            "company" => FILTER_VALIDATE_INT,
            "email" => FILTER_SANITIZE_EMAIL,
            "file" => FILTER_SANITIZE_STRING,
            "status" => FILTER_SANITIZE_STRING,
            "comment" => FILTER_SANITIZE_STRING
        ]);
        // Possible get requests parameters
        $this->get = filter_input_array(INPUT_GET, [
            "id" => FILTER_VALIDATE_INT,
            "name" => FILTER_SANITIZE_STRING
        ]);

        // Make params user accessible
        if ($this->post) {
          $this->request = array_merge($this->request, $this->post);
        }
        if ($this->get) {
          $this->request = array_merge($this->request, $this->get);
        }
    }

    // Upload file on server
    function doUpload($file, $filePath) {
        if (!is_uploaded_file($file)) {
            return false;
        }

        move_uploaded_file($file, $filePath);
        return true;
    }
}
<?php
/** Autoloading The required Classes **/
require_once 'CoreController.php';

class TaskController extends CoreController
{
    private $model;

    /**
    * @param $file;
    */
    function __construct($file)
    {
        parent::__construct();

        /** Loading the corresponding Model class **/
        $this->model = new $file;

        if (!$this->isLoggedIn()) {
            $this->redirect('login');
        }
    }

    /**
    * load task list view with all tasks
    */
    public function index()
    {
        // Get Tasks List, depending on the role
        if ($this->role == $this->rolePropertyOwner){

            $tasks = $this->model->getPropertyOwnerTasks($_SESSION['user_id']);
            $view = 'index';

        } else if ($this->role == $this->roleCleaningCompany) {

            $tasks = $this->model->getCleaningCompanyTasks($_SESSION['user_id']);
            $view = 'company/index';

        }
        Init::view($view, ['tasks' => $tasks]);
    }

    /**
    * Load task details by Task Id
    */
    public function view(){

        // Get Task Details by Id
        $id = $this->request['id'];
        $task = $this->model->getTaskDetails($id);

        // Get Files uploaded by Property Owner
        $propertyOwnerFiles = $this->model->getTaskFiles($id, 'property_owner');

        // Get Files uploaded by Cleaning Company
        $cleaningCompanyFiles = $this->model->getTaskFiles($id, 'cleaning_company');

        if ($this->role == $this->rolePropertyOwner){

            $companies = $this->model->getAllCleaningCompanies();

            Init::view('viewtask', [
                'task' => $task, 
                'propertyOwnerFiles' => $propertyOwnerFiles, 
                'cleaningCompanyFiles'=> $cleaningCompanyFiles,
                'companies'=> $companies
            ]);

        } else {

            Init::view('company/viewtask', [
                'task' => $task, 
                'propertyOwnerFiles' => $propertyOwnerFiles, 
                'cleaningCompanyFiles'=> $cleaningCompanyFiles
            ]);
        }
    }

    /**
     * Assign task to company
     */
    public function assignTask(){
        return $this->model->assignTask($this->request['company'], $this->request['id']);
    }

    /**
     * Remove file
     */
    public function removeFile(){
        return $this->model->removeFile($this->request['id']);
    }

    /**
     * Upload Multiple files by Property Owner
     */
    public function uploadFile(){

        $data = [];
        $dir = '.' . $this->dirSeparator . $this->uploadDirPO;

        if (isset($_FILES['files'])) {

            // Proceed each file
            foreach ( $_FILES['files']['name'] as $i => $fileName ) {

                // Upload on server
                if ($this->doUpload($_FILES["files"]["tmp_name"][$i], $dir . $fileName)) {

                    $data[] = [
                        'task_id'=> $this->request['id'], 
                        'file_title'=> $this->request['file'], 
                        'path'=> $this->url . $this->uploadDirPO . $fileName, 
                        'type'=> pathinfo($fileName, PATHINFO_EXTENSION),
                        'user_id'=> $this->user_id, 
                        'doc_type'=> 'property_owner'
                    ];
                }
            }
        }

        // Add file record into database
        if (count($data)>0) {
            $this->model->uploadFile($data);
        }
        echo json_encode(array('count' => count($data)));
    }

    /**
     * Task status update with file upload
     */
    public function statusUpdate(){
        
        $this->model->setTaskStatus($this->request['status'], $this->request['id']);

        $data = [];
        $dir = '.' . $this->dirSeparator . $this->uploadDirCC;

        if (isset($_FILES['files'])){

            // Proceed each file
            foreach ( $_FILES['files']['name'] as $i => $fileName ){

                // Upload on server
                if ( $this->doUpload($_FILES["files"]["tmp_name"][$i], $dir . $fileName) ){

                    $data[] = [
                        'task_id'=> $this->request['id'], 
                        'file_title'=> $this->request['comment'], 
                        'path'=> $this->url . $this->uploadDirCC . $fileName,
                        'type'=> pathinfo($fileName, PATHINFO_EXTENSION), 
                        'user_id'=> $this->user_id, 
                        'doc_type'=> 'cleaning_company'
                    ];
                }
            }
        }
        
        // Add file record into database
        if (count($data)>0) {
            $this->model->uploadFile($data);
        }
        echo json_encode(array('count' => count($data)));
    }

    /**
     * Logout user
     */
    public function logout(){
        session_destroy();
        $this->redirect('login');
    }
}
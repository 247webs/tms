<?php
/** Autoloading The required Classes **/
require_once 'DatabaseModel.php';

class TaskModel extends DatabaseModel
{
    /**
     * Create object of DatabaseModel class
     */
    function __construct(){
        parent::__construct();
    }

    /**
     * Get all tasks
     */
    public function getPropertyOwnerTasks($propertyOwnerId){

        $query = "SELECT 
                    task.id, 
                    task.title, 
                    task.description, 
                    task.created_at,
                    task_assigned.status,
                    users.name
                FROM
                    task
                LEFT JOIN 
                    task_assigned 
                ON 
                    task_assigned.task_id = task.id 
                LEFT JOIN 
                    users 
                ON 
                    users.id = task_assigned.cleaning_company_id 
                WHERE 
                    task.user_id=" . $propertyOwnerId . "
                ORDER BY 
                    id ASC";

        return $this->getMultipleRecords($query);
    }

    /**
     * Get tasks by comapny id
     * @param $id;
     */
    public function getCleaningCompanyTasks($cleaningCompanyId){

        $task = "SELECT 
                    task.id, 
                    task.title, 
                    task.description, 
                    task.created_at, 
                    task_assigned.status, 
                    task_assigned.assigned_date 
                FROM 
                    task
                LEFT JOIN 
                    task_assigned 
                ON 
                    task_assigned.task_id = task.id 
                WHERE 
                    task_assigned.cleaning_company_id=" . $cleaningCompanyId . "
                ORDER BY 
                    id ASC";

        return $this->getMultipleRecords($task);
    }

    /**
     * Get task details by Id
     * @param $id;
     */
    public function getTaskDetails($id){

        $query = "SELECT 
                    task.id, 
                    task.title,
                    task.description,
                    task.created_at,
                    task_assigned.cleaning_company_id,
                    task_assigned.status, 
                    users.name as comapny_name
                FROM 
                    task 
                LEFT JOIN 
                    task_assigned 
                on 
                    task_assigned.task_id=task.id 
                LEFT JOIN 
                    users 
                ON
                    users.id = task_assigned.cleaning_company_id 
                WHERE 
                    task.id =".$id;

        $row = $this->getSingleRecord($query);

        return $row;
    }

    /**
     * Get all assets of a task
     * @param $id;
     * @param $doc_type;
     */
    public function getTaskFiles($id, $doc_type) {

        $query = "SELECT * FROM `task_assets` WHERE task_id =".$id." and doc_type= '".$doc_type."'" ;

        return $this->getMultipleRecords($query);
    }

    /**
     * Get all cleaning companies
     */     
    public function getAllCleaningCompanies(){
        
        $query = "SELECT * FROM users WHERE role_id=" . $this->roleCleaningCompany;

        return $this->getMultipleRecords($query);
    }


    /**
     * Update or add record
     * @param $company;
     * @param $id;
     */
    public function assignTask($company, $id){

        // If task is set as unassigned        
        if ($company == 0) {
            $query = "DELETE FROM task_assigned WHERE task_id='".$id."'";
            return $this->execute($query);
        }

        // If a cleaning company is assigned
        $query = "SELECT * FROM task_assigned WHERE task_id =" . $id;
        $row = $this->getSingleRecord($query);
        if (count($row) > 0) {
            $qry = "UPDATE 
                        task_assigned
                    SET 
                        cleaning_company_id='".$company."', 
                        assigned_date='".date('y-m-d H:i:s')."' 
                    WHERE 
                        task_id='".$id."'";
        } else {
            $qry = "INSERT INTO 
                        task_assigned 
                        (task_id, cleaning_company_id, assigned_date, status) 
                    VALUES 
                        ('".$id."', '".$company."', '".date('y-m-d H:i:s')."', 'Assigned')";
        }

        $msg = $this->execute($qry);

        return $msg;
    }

    /**
     * Remove File
     * @param $id;
     */
    public function removeFile($id){
        $qry = "DELETE FROM task_assets WHERE id = ".$id;
        return $this->execute($qry);
    }

    /**
     * Upload multiple files
     * @param $data;
     */
    public function uploadFile($data){
        for($i=0; $i<count($data);$i++){
            $query = "INSERT INTO 
                        task_assets (task_id, title, asset_type, asset_name, user_id, doc_type, created_at) 
                    VALUES 
                        (
                            '".$data[$i]['task_id']."', 
                            '".$data[$i]['file_title']."', 
                            '".$data[$i]['type']."', 
                            '".$data[$i]['path']."', 
                            '".$data[$i]['user_id']."', 
                            '".$data[$i]['doc_type']."',
                            '".date('Y-m-d h:i:s')."'
                        )";
            $this->execute($query);
        }
        return true;
    }

    /**
     * Update task status
     * @param $status;
     * @param $task_id;
     */
    public function setTaskStatus($status, $task_id){        
        $query = "UPDATE task_assigned SET status='".$status."' WHERE task_id=".$task_id;
        return $this->execute($query);
    }
}
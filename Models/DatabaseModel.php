<?php
/** Autoloading The required Classes **/
require_once 'Config/Config.php';

class DatabaseModel extends Config
{
    public $con;

    /**
     *Create db connection
     */

    function __construct(){

        parent::__construct();

        $this->con = mysqli_connect(
            $this->database_host, 
            $this->database_username, 
            $this->database_password, 
            $this->database_name, 
            $this->database_port
        );
    }

    /**
     * Get Single Record
     *@param $query;
     */

    public function getSingleRecord($query){

        $row = [];
        $result = mysqli_query($this->con, $query);
        if (mysqli_num_rows($result)>0){
            $row = mysqli_fetch_assoc($result);
        }
        return $row;
    }

    /**
     *execute query and return all records
     *@param $query;
     */
    public function getMultipleRecords($query){
        $return = [];
        $result = mysqli_query($this->con, $query);
        while($row = mysqli_fetch_assoc($result)){
            $return[] = ($row);
        }
        return $return;
    }

    /**
     * execute query and return success or file message
     *@param $qry;
     */
    public function execute($query){
        $result = mysqli_query($this->con, $query);
        return $result ? "success" : "fail";
    }
}
<?php
/** Autoloading The required Classes **/
require_once 'DatabaseModel.php';

class UserModel extends DatabaseModel
{
    /**
     *Create object of DatabaseModel Class
     */
    function __construct(){

        parent::__construct();

    }

    /**
     * get user by email
     * @param $email;
     */
    public function login($email){

        $query = "SELECT * FROM users WHERE email='".$email."'";
        $row = $this->getSingleRecord($query);
        if(count($row)>0) {
            $_SESSION['user_id'] = $row['id'];
            $_SESSION['email'] = $row['email'];
            $_SESSION['role'] = $row['role_id'];
            return true;
        }
        return false;
    }
}
